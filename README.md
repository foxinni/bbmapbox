# Bluebird - Mapbox Sandbox

This project is a simple React application that displays some intersting features of the Mapbox Api.

## About

It roughly features the following:
* Map Styles (via the Mapbox styles api)
* 3D features of Mapbox
* 3D objects inside of Mapbox - using THREEJS and GLTF

## Installation
1. `npm install`
1. `npm run dev`
1. goto `localhost:3000`

## Troubleshooting
If the maps wont load there are two possible issues:
* Check that the API key is still valid. It using my personal (foxinni) keys and may get revoked over time.
* Check that the maps style is still valid, as this may also have been revoked or deleted over time.

