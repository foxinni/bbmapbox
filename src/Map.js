import React, { useEffect, useState } from "react";
import mapboxgl from "mapbox-gl";
export default function Map() {

  const [lat,] = useState(49.278788); // 50 Electric Avenue
  const [lng,] = useState(-122.838406); // 50 Electric Avenue

  const [zoom,] = useState(18);

  const myRef = React.createRef();

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: myRef.current,
      // style: "mapbox://styles/mapbox/streets-v11",
      // style: 'mapbox://styles/mapbox/light-v10',
      style: "mapbox://styles/foxinni/ckhbveew70w3r19rxjtyqvss7",
      center: [lng, lat],
      zoom: zoom,
      pitch: 65,
      antialias: true,
      // bearing: -17.6,
    });

    // The 'building' layer in the mapbox-streets vector source contains building-height
    // data from OpenStreetMap.
    map.on("load", function() {
      // Insert the layer beneath any symbol layer.
      var layers = map.getStyle().layers;

      var labelLayerId;
      for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === "symbol" && layers[i].layout["text-field"]) {
          labelLayerId = layers[i].id;
          break;
        }
      }

      map.addLayer(
        {
          id: "3d-buildings",
          source: "composite",
          "source-layer": "building",
          filter: ["==", "extrude", "true"],
          type: "fill-extrusion",
          minzoom: 15,
          paint: {
            "fill-extrusion-color": "#aaa",
            // use an 'interpolate' expression to add a smooth transition effect to the
            // buildings as the user zooms in
            "fill-extrusion-height": [
              "interpolate",
              ["linear"],
              ["zoom"],
              15,
              0,
              15.05,
              ["get", "height"],
            ],
            "fill-extrusion-base": [
              "interpolate",
              ["linear"],
              ["zoom"],
              15,
              0,
              15.05,
              ["get", "min_height"],
            ],
            "fill-extrusion-opacity": 0.6,
          },
        },
        labelLayerId
      );
    });
  });

  return <div ref={myRef} className="mapContainer" />;
}
