import React, { useEffect, useState, useRef } from "react"
import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from "mapbox-gl"
import * as THREE from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

// @ts-ignore
// eslint-disable-next-line import/no-webpack-loader-syntax
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;

export default function Map3dA() {
  const mapRef = useRef(null);

  const [lat,] = useState(49.17919);
  const [lng,] = useState(-123.11486);

  const [zoom,] = useState(11.5)
  const [pitch,] = useState(45)
  const [bearing,] = useState(0)

  useEffect(() => {
    var map = new mapboxgl.Map({
      container: mapRef.current,
      zoom: zoom,
      center: [lng, lat],
      pitch: pitch,
      bearing: bearing,
      style: 'mapbox://styles/foxinni/ckhbveew70w3r19rxjtyqvss7',
      // style: 'mapbox://styles/mapbox-map-design/ckhqrf2tz0dt119ny6azh975y'
      // style: "mapbox://styles/mapbox/streets-v11",
    });

    // parameters to ensure the model is georeferenced correctly on the map
    var modelOrigin = [lng, lat];
    var modelAltitude = 0;
    var modelRotate = [Math.PI / 2, 0, 0];

    var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
      modelOrigin,
      modelAltitude
    );

    // transformation parameters to position, rotate and scale the 3D model onto the map
    var modelTransform = {
      translateX: modelAsMercatorCoordinate.x,
      translateY: modelAsMercatorCoordinate.y,
      translateZ: modelAsMercatorCoordinate.z,
      rotateX: modelRotate[0],
      rotateY: modelRotate[1],
      rotateZ: modelRotate[2],
      /* Since our 3D model is in real world meters, a scale transform needs to be
      * applied since the CustomLayerInterface expects units in MercatorCoordinates.
      */
      scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits() * 400
    };

    var model
    // var ship_material = new THREE.MeshBasicMaterial({ color: 0x444444 });
    var lammat = new THREE.MeshLambertMaterial({
      color: 0xffffff,
      side: THREE.DoubleSide
    });
    // configuration of the custom layer for a 3D model per the CustomLayerInterface
    var customLayer = {
      id: '3d-model',
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map, gl) {
        this.camera = new THREE.Camera();
        this.scene = new THREE.Scene();

        // create two three.js lights to illuminate the model
        var directionalLight = new THREE.DirectionalLight(0xff00ff);
        directionalLight.position.set(0, -70, 100).normalize();
        this.scene.add(directionalLight);

        var directionalLight2 = new THREE.DirectionalLight(0x2200ff);
        directionalLight2.position.set(0, 70, 100).normalize();
        this.scene.add(directionalLight2);

        // use the three.js GLTF loader to add the 3D model to the three.js scene
        // var loader = new THREE.GLTFLoader();
        var loader = new GLTFLoader();

        loader.load(
          // 'https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf',
          './onelogo.gltf',
          function (gltf) {
            model = gltf.scene
            model.traverse(function (child) {
              if (child instanceof THREE.Mesh) {
                child.material = lammat;
              }
            });
            this.scene.add(model);
          }.bind(this)
        );
        this.map = map;

        // use the Mapbox GL JS map canvas for three.js
        this.renderer = new THREE.WebGLRenderer({
          canvas: map.getCanvas(),
          context: gl,
          antialias: true
        });

        this.renderer.autoClear = false;
      },
      render: function (gl, matrix) {
        var rotationX = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(1, 0, 0),
          modelTransform.rotateX
        );
        var rotationY = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(0, 1, 0),
          modelTransform.rotateY
        );
        var rotationZ = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(0, 0, 1),
          modelTransform.rotateZ
        );

        var m = new THREE.Matrix4().fromArray(matrix);
        var l = new THREE.Matrix4()
          .makeTranslation(
            modelTransform.translateX,
            modelTransform.translateY,
            modelTransform.translateZ
          )
          .scale(
            new THREE.Vector3(
              modelTransform.scale,
              -modelTransform.scale,
              modelTransform.scale
            )
          )
          .multiply(rotationX)
          .multiply(rotationY)
          .multiply(rotationZ);

        if (model) model.rotation.y += 0.01;

        this.camera.projectionMatrix = m.multiply(l);
        this.renderer.state.reset();
        this.renderer.render(this.scene, this.camera);
        this.map.triggerRepaint();
      }
    };

    map.on('style.load', function () {
      map.addLayer(customLayer);
    });

    map.on('mousemove', () => {
      console.log(map.isMoving())
      // Fly to a random location by offsetting the point -74.50, 40
      // by up to 5 degrees.
    })

    map.on('load', function () {
      map.addSource('mapbox-dem', {
        'type': 'raster-dem',
        'url': 'mapbox://mapbox.mapbox-terrain-dem-v1',
        'tileSize': 512,
        'maxzoom': 14
      });
      // add the DEM source as a terrain layer with exaggerated height
      map.setTerrain({ 'source': 'mapbox-dem', 'exaggeration': 1.5 });

      // add a sky layer that will show when the map is highly pitched
      map.addLayer({
        'id': 'sky',
        'type': 'sky',
        'paint': {
          'sky-type': 'atmosphere',
          'sky-atmosphere-sun': [0.0, 0.0],
          'sky-atmosphere-sun-intensity': 15
        }
      });

      // var lastTime = 0.0;
      function frame(time) {
        // animationIndex %= animations.length;
        // var current = animations[animationIndex];

        // if (animationTime < current.duration) {
        //   current.animate(animationTime / current.duration);
        // }

        // // allow requestAnimationFrame to control the speed of the animation
        // animationTime += 1 / (time - lastTime);
        // lastTime = time;

        // if (animationTime > current.duration) {
        //   animationIndex++;
        //   animationTime = 0.0;
        // }
        // modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
        //   modelOrigin,
        //   modelAltitude
        // );
        // modelTransform.rotateY = modelTransform.rotateY + 0.01

        // transformation parameters to position, rotate and scale the 3D model onto the map
        // modelTransform = {
        //   translateX: modelAsMercatorCoordinate.x,
        //   translateY: modelAsMercatorCoordinate.y,
        //   translateZ: modelAsMercatorCoordinate.z,
        //   rotateX: modelRotate[0],
        //   rotateY: modelRotate[1],
        //   rotateZ: modelRotate[2],
        //   /* Since our 3D model is in real world meters, a scale transform needs to be
        //   * applied since the CustomLayerInterface expects units in MercatorCoordinates.
        //   */
        //   scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
        // };

        window.requestAnimationFrame(frame);
      }

      window.requestAnimationFrame(frame);

      const timer = window.setInterval(() => {
        console.log('3 seconds has passed')

        var center = map.getCenter()
        var zoom = map.getZoom()
        var pitch = map.getPitch()
        var bearing = map.getBearing()
        // access longitude and latitude values directly
        var { lng, lat } = center;
        console.log(lng, lat, zoom, pitch, bearing)
      }, 3000);
      return () => { // Return callback to run on unmount.
        window.clearInterval(timer);
      };

    });

  });

  return [<div ref={el => (mapRef.current = el)} className="mapContainer" />, <div className="coords">{lng},{lat}###zoom:{zoom}###potch{pitch}###bearing{bearing}</div>]
}
