import React from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import "./App.css";

import mapboxgl from "mapbox-gl";

import Map from "./Map";
import Map3d from "./Map3d";
import Map3dA from "./Map3dA";

mapboxgl.accessToken =
  "pk.eyJ1IjoiZm94aW5uaSIsImEiOiJja2txdXlleTQwOG5iMnVxaHkzYWUwazd2In0.1B3TW-NlAVyZi9gz9CEXow";

function App() {
  return (
    <Router>
      <header>
        <h1>Bluebird Mapbox</h1>
        <Link to="/">Home</Link>
        <Link to="/map">Map</Link>
        <Link to="/3d">3D</Link>
        <Link to="/3da">3DA</Link>
      </header>

      <Switch>
        <Route exact path="/">
          <div className="page">
            <h1>Welcome. Use the menu above to get started.</h1>
          </div>
        </Route>
        <Route path="/map">
          <div className="page">
            <Map />
          </div>
        </Route>
        <Route path="/3d">
          <div className="page">
            <Map3d />
          </div>
        </Route>
        <Route path="/3da">
          <div className="page">
            <Map3dA />
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
